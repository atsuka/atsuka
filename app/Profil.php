<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profils';
    protected $fillable = [
        'id_jsresume',
        'nama_lengkap',
        'nama_profisi',
        'email',
        'notlp',
        'website',
        'tentang_saya',
        'tanggal_lahir',
        'jenis_kelamin',
        'bahasa',
        'alamat',
        'kemampuan',
    ];
}
