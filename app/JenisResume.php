<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisResume extends Model
{
    protected $table = 'jenis_resumes';
    protected $fillable = [
        'kode_resume',
        'nama_resume',
    ];

}
