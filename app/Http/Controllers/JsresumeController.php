<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisResume;

class JsresumeController extends Controller
{
    public function index()
    {
        $data['jsresume'] = JenisResume::all();
        return view('jsresume.index', $data);
    }
    public function create()
    {
        return view('jsresume.create');
    }
    public function store(Request $req)
    {
         $data = new JenisResume;
         $data->kode_resume = $req->kode_resume;
         $data->nama_resume = $req->nama_resume;
         $data->save();
         return redirect('home');
    }
    public function edit($id)
    {
        $data['jsresume'] = JenisResume::find($id);
        return view('jsresume.edit', $data);
    }
    public function update(Request $req)
    {
        $data = JenisResume::where('kode_resume', $req->kode_resume)->update([
            'nama_resume'=>$req->nama_resume
        ]);
        return back();
    }
    public function delete()
    {
       
    }
}
