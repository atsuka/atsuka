<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisResume;
use App\Profil;

class ProfilController extends Controller
{
    public function index()
    {
        $data['profil'] = Profil::all();
        return view('profil.index', $data);
    }
    public function create()
    {
        $data['jresume'] = JenisResume::all();
        return view('profil.create', $data);
    }
    public function store(Request $req)
    {
       
        $data =  $req->all();
        Profil::create($data);
        return $back;
    }
}
