<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisResume;

class ResumeController extends Controller
{
    public function index()
    {
        return view('resume.index');
    }
    public function create()
    {
        $data['jenis_resume'] = JenisResume::all();
        return view('resume.create', $data);
    }
}
