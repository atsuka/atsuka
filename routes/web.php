<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
git*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('jenisresume/index', 'JsresumeController@index')->name('jsresume.index');
Route::get('jenisresume/create', 'JsresumeController@create')->name('jsresume.create');
Route::post('jenisresume/store', 'JsresumeController@store')->name('jsresume.store');
Route::get('jenisresume/edit{id}', 'JsresumeController@edit')->name('jsresume.edit');
Route::post('jenisresume/update', 'JsresumeController@update')->name('jsresume.update');
Route::get('jenisresume/delete/{id}', 'JsresumeController@delete')->name('jsresume.delete');
//fungsi profil
Route::get('profil', 'ProfilController@index')->name('profil.index');
Route::get('profil/create', 'ProfilController@create')->name('profil.create');
Route::post('profil/store', 'ProfilController@store')->name('profil.store');
//fungsi resume
Route::get('resume', 'ResumeController@index')->name('resume.index');
Route::get('resume/create', 'ResumeController@create')->name('resume.create');