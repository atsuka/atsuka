<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profils', function (Blueprint $table) {
            $table->id();
            $table->integer('id_jsresume');
            $table->string('nama_lengkap');
            $table->string('nama_profisi');
            $table->string('email');
            $table->string('notlp');
            $table->string('website');
            $table->text('tentang_saya');
            $table->string('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('bahsasa');
            $table->text('alamat');
            $table->string('kemampuan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profils');
    }
}
