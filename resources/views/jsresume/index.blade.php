@extends('layouts.dashboard')
@section('center')
    <div class="card card-primary">
        <div class="card-header">
            <h4 class="card-panel">Jensi Resume</h4>
        </div>
        <div class="body">
            <table class="table table-bordered">
                <thead>
                    <th>ID</th>
                    <th>Kode Resume</th>
                    <th>Resume</th>
                    <th>Menu</th>
                </thead>
                <tbody>
                    @foreach ($jsresume as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->kode_resume}}</td>
                            <td>{{$item->nama_resume}}</td>
                            <td>
                                <a href="{{route('jsresume.edit', $item->id)}}" class="btn btn-success">
                                    Edit
                                </a>
                                <a href="{{route('jsresume.delete', $item->id)}}" class="btn btn-danger">
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection