@extends('layouts.dashboard')
@section('center')

   {!! Form::open(array('url'=>'jenisresume/update')) !!}
     <table class="table">
        <tr>
            <td>Kode Resume</td>
            <td><input type="text" class="form-control" value="{{$jsresume->kode_resume}}" name="kode_resume"></td>
        </tr>
        <tr>
            <td> Resume</td>
            <td><input type="text" class="form-control" value="{{$jsresume->nama_resume}}" name="nama_resume"></td>
        </tr>
        <tr>
            <td><button type="submit" class="btn btn-primary">Ok</button></td>
        </tr>
    </table>
{!! Form::close() !!}

@endsection