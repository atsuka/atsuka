@extends('layouts.dashboard')
@section('center')
{!! Form::open(array('url'=>'jenisresume/store')) !!}
<table class="table">
    <tr>
        <td>Kode Resume</td>
        <td><input type="text" class="form-control" name="kode_resume"></td>
    </tr>
    <tr>
        <td>Nama Resume</td>
        <td><input type="text" class="form-control" name="nama_resume"></td>
    </tr>
   <tr>
       <td><button type="submit" class="btn btn-primary">OK</button></td>
   </tr>
</table>
{!! Form::close() !!}
@endsection