@extends('layouts.dashboard')
@section('center')
    <table class="table">
        <thead>
            <th>ID Resume</th>
            <th>Nama Lenkap</th>
            <th>Email</th>
            <th>No Handpone</th>
            <th>Website</th>
            
            <th>menu</th>
        </thead>
        <tbody>
            @foreach ($profil as $v)
                <tr>
                    <td>{{$v->id_jsresume }}</td>
                    <td>{{$v->nama_lengkap }}</td>
                    <td>{{$v->email }}</td>
                    <td>{{$v->notlp }}</td>
                    <td>{{$v->website }}</td>
                    <td>
                        <a href="" class="btn btn-primary">
                            <i>Edit</i>
                        </a>
                        <a href="" class="btn btn-danger">
                            <i>Delte</i>
                        </a>
                    </td>
                   
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection