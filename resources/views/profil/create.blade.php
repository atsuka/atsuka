@extends('layouts.dashboard')
@section('center')
{!! Form::open(array('url'=>'profil/store')) !!}
    <table class="table">
        <tr>
            <td>
                <p>Jenis Resume</p>
                <select name="id_jsresume" class="form-control" id="">
                    <option value="">Pilih</option>
                    @foreach ($jresume as $v)
                        <option value="{{$v->kode_resume}}">{{$v->nama_resume}}</option>
                    @endforeach
                </select>
                
            </td>
            <td>
                <p>Nama Lengkap</p>
                <input type="text" class="form-control" name="nama_lengkap">
            </td>
            <td>
                <p>Profesi</p>
                <input type="text" class="form-control" name="nama_profisi">
            </td>
        </tr>
        <tr>
            <td>
                <p>Email</p>
                <input type="text" class="form-control" name="email">
            </td>
            <td>
                <p>No Handpone</p>
                <input type="text" class="form-control" name="notlp">
            </td>
            <td>
                <p>Website</p>
                <input type="text" class="form-control" name="website">
            </td>
        </tr>
        <tr>
            <td>
                <p>Jenis Kelamin</p>
                <input type="text" class="form-control" name="jenis_kelamin">
            </td>
            <td>
                <p>Bahasa</p>
                <input type="text" class="form-control" name="bahasa">
            </td>
            <td>
                <p>Tanggal Lahir</p>
                <input type="text" class="form-control" name="tanggal_lahir">
            </td>
        </tr>
        <tr>
            <td>
                <p>ALmat</p>
                <textarea name="alamat" id="" cols="30" rows="5" class="form-control">

                </textarea>
            </td>
            <td>
                <p>Tentang Saya</p>
                <textarea name="tentang_saya" id="" cols="30" rows="5" form-control></textarea>
            </td>
            <td>
                <p>Kemamapuan</p>
                <textarea name="kemampuan"  class="form-control" id="" cols="30" rows="5"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                {!! Form::submit('ok', ['class'=>'btn btn-primary']) !!}
                {!! link_to('home', 'kembali', ['class'=>'btn btn-danger']) !!}
            </td>
        </tr>
    </table>
{!! Form::close() !!}
@endsection