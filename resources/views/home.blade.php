@extends('layouts.dashboard')
@section('sidemenu')
<ul class="nav-main">
    <li class="nav-main-item">
        <a class="nav-main-link" href="be_pages_dashboard.html">
            <i class="nav-main-link-icon si si-speedometer"></i>
            <span class="nav-main-link-name">Dashboard</span>
        </a>
    </li>
    <li class="nav-main-heading">User Interface</li>
    <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
            <i class="nav-main-link-icon si si-energy"></i>
            <span class="nav-main-link-name">Resume</span>
        </a>
        <ul class="nav-main-submenu">
            <li class="nav-main-item">
                <a class="nav-main-link" href="{{route('jsresume.index')}}">
                    <span class="nav-main-link-name">Resume</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="">
                    <span class="nav-main-link-name">Tamabah Resume</span>
                </a>
            </li>
           
        </ul>
    </li>
    <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
            <i class="nav-main-link-icon si si-badge"></i>
            <span class="nav-main-link-name">Elements</span>
        </a>
        <ul class="nav-main-submenu">
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_grid.html">
                    <span class="nav-main-link-name">Grid</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_typography.html">
                    <span class="nav-main-link-name">Typography</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_icons.html">
                    <span class="nav-main-link-name">Icons</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_buttons.html">
                    <span class="nav-main-link-name">Buttons</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_buttons_groups.html">
                    <span class="nav-main-link-name">Button Groups</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_dropdowns.html">
                    <span class="nav-main-link-name">Dropdowns</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_tabs.html">
                    <span class="nav-main-link-name">Tabs</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_navigation.html">
                    <span class="nav-main-link-name">Navigation</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_navigation_horizontal.html">
                    <span class="nav-main-link-name">Horizontal Navigation</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_progress.html">
                    <span class="nav-main-link-name">Progress</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_alerts.html">
                    <span class="nav-main-link-name">Alerts</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_tooltips.html">
                    <span class="nav-main-link-name">Tooltips</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_popovers.html">
                    <span class="nav-main-link-name">Popovers</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_modals.html">
                    <span class="nav-main-link-name">Modals</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_images.html">
                    <span class="nav-main-link-name">Images Overlay</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_timeline.html">
                    <span class="nav-main-link-name">Timeline</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_ribbons.html">
                    <span class="nav-main-link-name">Ribbons</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_animations.html">
                    <span class="nav-main-link-name">Animations</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_ui_color_themes.html">
                    <span class="nav-main-link-name">Color Themes</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
            <i class="nav-main-link-icon si si-grid"></i>
            <span class="nav-main-link-name">Tables</span>
        </a>
        <ul class="nav-main-submenu">
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_tables_styles.html">
                    <span class="nav-main-link-name">Styles</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_tables_responsive.html">
                    <span class="nav-main-link-name">Responsive</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_tables_helpers.html">
                    <span class="nav-main-link-name">Helpers</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_tables_pricing.html">
                    <span class="nav-main-link-name">Pricing</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_tables_datatables.html">
                    <span class="nav-main-link-name">DataTables</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
            <i class="nav-main-link-icon si si-note"></i>
            <span class="nav-main-link-name">Forms</span>
        </a>
        <ul class="nav-main-submenu">
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_forms_elements.html">
                    <span class="nav-main-link-name">Elements</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_forms_custom_controls.html">
                    <span class="nav-main-link-name">Custom Controls</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_forms_layouts.html">
                    <span class="nav-main-link-name">Layouts</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_forms_input_groups.html">
                    <span class="nav-main-link-name">Input Groups</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_forms_plugins.html">
                    <span class="nav-main-link-name">Plugins</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_forms_editors.html">
                    <span class="nav-main-link-name">Editors</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_forms_validation.html">
                    <span class="nav-main-link-name">Validation</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_forms_wizard.html">
                    <span class="nav-main-link-name">Wizard</span>
                </a>
            </li>
        </ul>
    </li>

</ul>
@endsection
@section('content')

{{-- <div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
