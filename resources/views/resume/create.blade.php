@extends('layouts.dashboard')
@section('center')
<table class="table">
    <tr>
        <td>Jenis Resume</td>
        <td>
            <select name="id_jsresume" id="">
                <option value="">Pilih</option>
                @foreach ($jenis_resume as $v)
                    <option value="{{$v->id_resume}}">{{$v->nama_resume}}</option>
                @endforeach
            </select>
        </td>
    </tr>
    <tr>
        <td class="myTable">
            <p>Pendidikan</p>
            <input type="text" class="form-control" name="pendidikan[]">
        </td>
        <td>
            <p>Pengalaman Kerja</p>
            <input type="text" class="form-control" name="penglaman_kerja[]">
        </td>
        <td>
            <p>Penghargaan</p>
            <input type="text" class="form-control" name="penghargaan[]">
        </td>
    </tr>
</table>
<script>
    function myFunction() {
      var table = document.getElementById("myTable");
      var row = table.insertRow(0);
      var cell1 = row.insertCell(0);
      var cell2 = row.insertCell(1);
      cell1.innerHTML = "NEW CELL1";
      cell2.innerHTML = "NEW CELL2";
    }
    </script>
@endsection